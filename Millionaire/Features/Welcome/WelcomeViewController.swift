//
//  WelcomeViewController.swift
//  Millionaire
//
//  Created by Vladimir Fibe on 2/6/23.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var questionLabel: UILabel!
    let questions = Question.questions()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let question = questions.randomElement() {
            
            questionLabel.text = question.ask
        }
    }


    

}
